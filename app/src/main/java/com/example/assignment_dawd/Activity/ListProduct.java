package com.example.assignment_dawd.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AppCompatActivity;

import com.example.assignment_dawd.R;
import com.example.assignment_dawd.database.DBHelper;

public class ListProduct extends AppCompatActivity {
    private DBHelper db;
    private Cursor c;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_product);

        db = new DBHelper(this);
        ListView lvProduct = findViewById(R.id.lvProduct);

        c = db.getAllProduct();
        adapter = new SimpleCursorAdapter(this, R.layout.item_product, c,
                new String[]{DBHelper.ID, DBHelper.NAME, DBHelper.QUANTITY},
                new int[]{R.id.tvId, R.id.tvName, R.id.tvQuantity},
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        lvProduct.setAdapter(adapter);

        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Cursor cursor = (Cursor) adapter.getItem(position);
                int _id = cursor.getInt(cursor.getColumnIndex(DBHelper.ID));
                String name = cursor.getString(cursor.getColumnIndex(DBHelper.NAME));
                String quantity = cursor.getString(cursor.getColumnIndex(DBHelper.QUANTITY));

                Intent intent = new Intent(String.valueOf(ListProduct.this));
                intent.putExtra(DBHelper.ID, _id);
                intent.putExtra(DBHelper.NAME, name);
                intent.putExtra(DBHelper.QUANTITY, quantity);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        c = db.getAllProduct();
        adapter.changeCursor(c);
        adapter.notifyDataSetChanged();
        db.close();
    }

}

