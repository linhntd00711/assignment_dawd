package com.example.assignment_dawd.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.assignment_dawd.R;
import com.example.assignment_dawd.database.DBHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edName;
    private EditText edQuantity;
    private Button btADD;
    private Button btVIEW;
    private DBHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        db = new DBHelper(this);
        db.getReadableDatabase();

    }

    private void initView() {
        edName = findViewById(R.id.edName);
        edQuantity = findViewById(R.id.edQuantity);
        btADD = findViewById(R.id.btADD);
        btVIEW = findViewById(R.id.btVIEW);
        btADD.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == btADD) {
            onADD();
        }
        if(v==btVIEW){
            onVIEW();
        }
    }
    private void onADD() {
        if (edName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Name", Toast.LENGTH_LONG).show();
            return;
        }
        if (edQuantity.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter Quantity", Toast.LENGTH_LONG).show();
            return;
        }
        String isAdd = db.addProduct(edName.getText().toString(), edQuantity.getText().toString());
        Toast.makeText(this, isAdd, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(MainActivity.this, ListProduct.class);
        startActivity(intent);
    }

    private void onVIEW() {
        Intent intent = new Intent(MainActivity.this, ListProduct.class);
        startActivity(intent);
    }
}
